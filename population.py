#!bin/bash/python

import pandas as pd
import constants as constants


class WorldPopulation(object):
    def __init__(self):
        self.population = pd.read_csv(constants.F_POP, header=0)
        self.population.rename(columns={"Country Code": "ISO_Alpha_3_Country",
                                        "2019": "Country_Population"},
                               inplace=True)
        self.fixing_missing_values()
        self.population['Country_Population'] = self.population['Country_Population'].astype(int)
        

    def fixing_missing_values(self):
        self.population.loc[self.population['ISO_Alpha_3_Country'] == "ERI", 'ISO_Alpha_3_Country'] = 6081196

    def get_population_df(self) -> pd.DataFrame:
        """ returns the world population dataframe
        with country codes in ISO-Alpha-3 """
        return self.population


if __name__ == "__main__":
    print(WorldPopulation().get_population_df())
