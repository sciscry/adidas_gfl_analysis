""" Data Preparation Module for global supply chain Covid-19 impact to adidas AG"""

__version__ = '0.9'
__author__ = 'SciScry GmbH'
from typing import Union, List
import os
import requests
from datetime import date, timedelta, datetime

import numpy as np
import pandas as pd
import plotly.express as px
import pycountry_convert as pc

import constants
from population import WorldPopulation


class SupplierNetwork(object):
    """ base of a supply network related information for corporation """

    def __init__(self, f_src: str, dtype_df: dict, dtype_trims: dict,
                 cntry_mapping: dict, separator: str, decimal: str):
        """
        :type dtype: dict
        Dictionary with all data types that are part of the
        supplier information
        """
        self.country_mapping = constants.COUNTRY_MAPPING

        # check if local file of supply network is available
        sn_file_path = "Main_Supply_Network.csv"
        # check if today's file exists and load if available
        if os.path.isfile(sn_file_path):
            # Supply Network data has already been created
            print("Supply Network - data file has been found - loading existing data")
            self.df = pd.read_csv(sn_file_path, na_values=['', '#N/A', '#N/A N/A',
                                                           '#NA', '-1.#IND', '-1.#QNAN',
                                                           '-NaN', '-nan', '1.#IND',
                                                           '1.#QNAN', '<NA>', 'N/A',
                                                           'NULL', 'NaN', 'n/a',
                                                           'nan', 'null'],
                                  keep_default_na=False)
        else:
            self.df = pd.DataFrame(columns=dtype_df)
            # file handling variables
            self.sep = separator  # file separator
            self.dec = decimal
            # load the data from the defined file path
            self.setDf(source=f_src, dec=self.dec, sep=self.sep)
            # clean dataframe
            self.cleanDf()
            # update Continent
            self.updateContinent(constants.COUNTRY_COLUMN)

            # add Migrant Workers percentage
            self.addMigrantWorkerPercentage()
            # create supplier properties
            self.createSupplierProperties()
            # add financial kpis to supply network
            df_fin_kpi = self.add_financial_kpi(self.df, constants.REV_OM_2019,
                                                constants.CONTINENT_MAPPING,
                                                "Workers_Count")
            self.df = self.df.merge(df_fin_kpi[["Account_Name", "Revenue", "Operating_Margin"]],
                                    how="left", on="Account_Name")
            # create local file
            #print('where are continents', self.df['Continent'].unique())
            self.df.to_csv(sn_file_path, index=False)

        # check if trims file is available locally
        trims_file_path = "Trims_Network.csv"
        if os.path.isfile(trims_file_path):
            print("Trims Network - Data has been found - loading existing data")
            self.trims_network = pd.read_csv(trims_file_path,
                                             na_values=['', '#N/A', '#N/A N/A',
                                                        '#NA', '-1.#IND', '-1.#QNAN',
                                                        '-NaN', '-nan', '1.#IND',
                                                        '1.#QNAN', '<NA>', 'N/A',
                                                        'NULL', 'NaN', 'n/a',
                                                        'nan', 'null'],
                                             keep_default_na=False)
        else:
            self.trims_network = pd.DataFrame(columns=dtype_trims)
            #  create Trims network
            self.createTrimsNetwork()
            # create local file
            self.trims_network.to_csv(trims_file_path, index=False)

    @staticmethod
    def get_unique_values(df: pd.DataFrame, column: str) -> list:
        """ receives a dataframe and returns the unique values for the provided column """
        return df[column].unique()

    def setDf(self, source: str, dec: object = str, sep: object = str):
        assert isinstance(
            source, str), 'Please provide file path to source data'
        assert isinstance(sep, str) & len(
            sep) == 1, 'Please provide the separator literal'
        assert dec == '.' or dec == ',', 'Please provide either "." or "," as decimal separator'

        try:
            self.df = pd.read_csv(constants.F_SRC, sep=sep, decimal=dec)
        except:
            print('File', constants.F_SRC, 'could not be read.')

    def cleanDf(self):
        # remove index column after loading from file
        if 'Unnamed: 0' in list(self.df.columns):
            self.df.drop(columns='Unnamed: 0', inplace=True)
        self.df["Workers_Count"] = self.df["Workers_Count"].fillna(
            0).astype(float).astype(int)

    def getTrimsMask(self, selection: dict = None):
        if selection is None:  # base case - 1
            return self.trims_network
        elif len(selection) == 1:  # base case - 2
            sub_sel = selection.popitem()  # removing last element
            return self.trims_network[sub_sel[0]].isin(sub_sel[1])
        else:
            sub_sel = selection.popitem()  # removing one element
            return (self.trims_network[sub_sel[0]].isin(sub_sel[1])) & self.getTrimsMask(selection)

    def getTrimsNetwork(self, selection: dict = None):
        '''get trims with dictionary'''
        if selection is None:  # no selection provided
            return self.trims_network
        else:
            return self.trims_network[self.getTrimsMask(selection)]

    def updateContinent(self, country_column):
        '''
        take a dataframe that contains column with country names and adds continents as field

        Parameters
        ----------
        country_column: string
            which column to use as country information
        df : pandas dataframe
            pandas dataframe that has a field called.

        Returns
        -------
        None.

        '''
        for index, row in self.df.iterrows():
            # use ISO 3166-1 alpha-2 country name
            if row[country_column] in constants.COUNTRY_MAPPING:
                self.df.loc[index, 'ISO_Country'] = \
                    constants.COUNTRY_MAPPING[row[country_column]]
            else:
                self.df.loc[index, 'ISO_Country'] = row[country_column]

            try:
                #                print(f"ISO_Country: {self.df.loc[index,'ISO_Country']}")
                self.df.loc[index, 'ISO_Alpha_2_Country'] = pc.country_name_to_country_alpha2(
                    self.df.loc[index, 'ISO_Country'].lower(), cn_name_format="lower")
                self.df.loc[index, 'ISO_Alpha_3_Country'] = \
                    pc.country_name_to_country_alpha3(
                    self.df.loc[index, 'ISO_Country'].lower(), cn_name_format='lower')
            except KeyError:
                print("Could not find continent for:", row[country_column])
                self.df.loc[index, 'Continent'] = 'NA'
                self.df.loc[index, 'ISO_Alpha_2_Country'] = 'NA'
                self.df.loc[index, 'ISO_Alpha_3_Country'] = 'NA'
            else:
                self.df.loc[index, 'Continent'] = \
                    pc.country_alpha2_to_continent_code(
                    self.df.loc[index, 'ISO_Alpha_2_Country'])
                if self.df.loc[index, 'Continent'] == "":
                    print(f"Continent: {self.df.loc[index, 'Continent']}")
                    print(
                        f"ISO Alpha 2 Country: {self.df.loc[index, 'ISO_Alpha_2_Country']}")

    def get_mask(self, df: pd.DataFrame, selection: dict = None):
        if selection is None:  # base case - 1
            return self.df
        elif len(selection) == 1:  # base case - 2
            sub_sel = selection.popitem()  # removing last element
            return (df[sub_sel[0]].isin(sub_sel[1]))
        else:
            sub_sel = selection.popitem()  # removing last element
            return ((df[sub_sel[0]].isin(sub_sel[1]))
                    & self.get_mask(df, selection))

    def get_df(self, df: pd.DataFrame, selection: dict = None):
        if selection is None:
            return df
        else:
            return df[self.get_mask(df, selection)]

    def add_financial_kpi(self, df: pd.DataFrame, financials: dict,
                          regions_mapping: dict, distribution_column: str):
        """ takes the financials kpi per region and distributes
        them among each T1 factory for based on key provided """
        kpi_df = pd.DataFrame()
        for key in regions_mapping:
            selection = {"Continent": regions_mapping[key]}
            selection.update({"Tier_Level": ["T1"]})
            region_factory_df = self.get_df(df, selection)
            dist_key_total = df[distribution_column].sum()
            for kpi in financials[key]:
                kpi_total = financials[key][kpi]
                region_factory_df[kpi] = kpi_total * \
                    region_factory_df[distribution_column] / dist_key_total
            kpi_df = kpi_df.append(region_factory_df)

        return kpi_df
    
    def addMigrantWorkerPercentage(self):
        '''
        add the percentage of migrant workers to the passed dataframe. 
        Assumes columns 'Migrant workers'

        Parameters
        ----------
        df : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        '''
        for index, row in self.df.iterrows():
            if self.df.loc[index, 'Workers_Count'] == 0:  # avoid division by zero cases
                self.df.loc[index, 'Migrant_Percentage'] == 0.0
            else:
                self.df.loc[index, 'Migrant_Percentage'] = (self.df.loc[index, 'Migrant_Workers'] /
                                                            self.df.loc[index, 'Workers_Count']) * 100

    def createSupplierProperties(self):
        '''generate information what type of supplier and which level supplier has'''

        # identify supplier level
        # identify and define Tier 1 suppliers
        mask = self.df['Tier'].str.contains(r'T1', na=False)  # T1
        self.df.loc[mask, 'Tier_Level'] = 'T1'
        mask = self.df['Tier'].str.contains(r'Tier 1', na=False)  # Tier 2
        self.df.loc[mask, 'Tier_Level'] = 'T1'

        # identify and define Tier 2 suppliers
        mask = self.df['Tier'].str.contains(r'T2', na=False)  # T2
        self.df.loc[mask, 'Tier_Level'] = 'T2'
        mask = self.df['Tier'].str.contains(r'Tier 2', na=False)  # Tier 2
        self.df.loc[mask, 'Tier_Level'] = 'T2'

        # identify supplier type
        # identify and define primary suppliers
        mask = self.df['Tier'].str.contains(
            r'Primary', na=False)  # finding Primary
        self.df.loc[mask, 'Supplier_Type'] = 'Primary'

        # identify and define subcontractor suppliers
        mask = self.df['Tier'].str.contains(
            r'Subcontractor', na=False)  # finding Subcontractor
        self.df.loc[mask, 'Supplier_Type'] = 'Primary'

        # identify and define material suppliers
        mask = self.df['Tier'].str.contains(
            r'Material', na=False)  # finding Material
        self.df.loc[mask, 'Supplier_Type'] = 'Material'

        # identify and define Trims suppliers
        mask = self.df['Tier'].str.contains(
            r'Trims', na=False)  # finding Trims
        self.df.loc[mask, 'Supplier_Type'] = 'Trims'

    def createTrimsNetwork(self):
        '''create global supply chain network based on rule set:
            Trims deliver all over the world in their product categories,
        except accessories which have trims'''
        df_t1_subset = pd.DataFrame()
        df_trims = self.df.loc[self.df['Supplier_Type']=='Trims']
        for category in ['Apparel', 'Footwear']:
            df_trims_category = df_trims.loc[self.df['Product_Categories'] \
                                .str.contains(category)]
            df_trims_category.reset_index(inplace=True)
            no_trims = len(df_trims_category.index)
            df_temp = self.df.loc[(self.df['Tier_Level']=='T1')\
                                    & (self.df['Product_Categories']\
                                    .str.contains(category))]
            if no_trims == 1:
                df_temp['Trims_Supplier'] = df_trims_category.loc[0, 'Account_Name']
                df_t1_subset = df_t1_subset.append(df_temp)
                #print('len == 1 shape', df_t1_subset.shape)
            else:
                df_temp.reset_index(inplace=True)
                for index, row in df_temp.iterrows():
                    # choose random supplier
                    trims_idx_pos = index % no_trims
                    df_temp.loc[index, 'Trims_Supplier'] =\
                    df_trims_category.loc[trims_idx_pos, 'Account_Name']
                df_t1_subset = df_t1_subset.append(df_temp)

        self.trims_network = df_t1_subset.merge(df_trims, how='inner',
                                           left_on='Trims_Supplier',
                                           right_on='Account_Name',
                                           suffixes=['_Tgt', '_Src'])
        self.trims_network.to_csv('./trims_test_network.csv', index=False)

class CountriesGeoJSON(object):
    def __init__(self):
        self.countries = self.loadCountryGeoJSON(constants.CNTRY_GEOJSON)

    def loadCountryGeoJSON(self, url):
        geoJSON = dict()
        geoJSON = requests.get(url).json()
        for elm in geoJSON['features']:
            elm['id'] = elm['properties']['ISO_A3']
        return geoJSON

    def getCountriesGeoJSON(self):
        return self.countries

    def createTrimsGeoJSON(self, df):
        pass


class CSSE_COVID_19(object):
    """ Corona data from JHU CSSE """
    def __init__(self):
        # constants for file locations and hard coded values
        self.csse_data_path = constants.RAW_CSSE_PATH
        self.cc_path = constants.COUNTRY_CODES_PATH
        self.cntry_clm = constants.CSSE_CNTRY_CLMN
        self.csse_groupby = constants.CSSE_GROUPBY

        # locally stored data handling
        yesterday = (date.today()-timedelta(days=1)).isoformat()
        today = date.today().isoformat()
        yesterdays_file_path = "./" + yesterday + "_CSSE_COVID_19_data.csv"
        todays_file_path = "./" + today + "_CSSE_COVID_19_data.csv"

        # check if today's file exists and load if available
        if os.path.isfile(todays_file_path):
            # CSSE data has already been loaded today
            print("CSSE - data file has been found - loading existing data")
            self.df_csse = pd.read_csv(todays_file_path, parse_dates=['Date'],
                                       date_parser = pd.to_datetime,
                                       cache_dates=True)
            self.df_cc = self.loadDf(self.cc_path)
        else: # no file exists create from scratch
            print("Loading Data from scratch")
            # loading current data from git
            self.df_csse = self.loadDf(self.csse_data_path)
            print("Preprocessing Data")
            self.df_csse = self.preprocessCSSEData(
                self.df_csse)  # preprocessing steps
            print("Calculation KPI")
            print("Daily New Cases KPI")
            self.df_csse = self.calcDailyNewCases(self.df_csse,
                                                  self.csse_groupby)
            ### replace negative new cases 
            self.df_csse.loc[self.df_csse['New_Cases'] < 0., 'New_Cases'] = 0.
            print(self.df_csse['New_Cases'].describe())
            print("Loading & Mapping ISO-3 Country Codes")
            self.df_csse = self.remap(self.df_csse, self.cntry_clm,
                                      constants.COUNTRY_MAPPING)  # remap country names for ISO-3166-Alpha-3 identification
            self.df_cc = self.loadDf(self.cc_path)
            self.df_csse = self.df_csse.merge(self.df_cc,
                                              left_on="Country/Region",
                                              right_on="COUNTRY", how="left")
            # add world population per country
            print("Adding Population")
            self.add_world_population()
            # create comparable KPI by scaling by population
            print("Calculating relative KPI")
            self.df_csse = self.calculate_relative_kpi(df=self.df_csse,
                                                       columns=constants.KPI_TO_SCALE,
                                                       scale_key="100k",
                                                       population_clmn="Country_Population")
            print("Calculating SMA 7 - new cases")
            self.df_csse = self.calc_sma(df=self.df_csse,
                                         columns=constants.SMA_KPI,
                                         period=7)
            print("Calculating - new cases SMA 7 - weekly growth")
            self.df_csse = self.calc_percentage_change(self.df_csse,
                                                       constants.PERCENTAGE_CHANGE_KPI, 7)
            print("Calculating - weekly incidence")
            self.df_csse = self.calc_incidence(self.df_csse)
            
            # create today's file
            print("creating file for loaded data")
            self.df_csse.to_csv(todays_file_path, index=False, date_format="%Y-%m-%d")

            # test if there is yesterday's file and remove it
            if os.path.isfile(yesterdays_file_path):
                os.remove(yesterdays_file_path)
            else:
                pass
            print("successful init")

    def add_world_population(self):
        """ adds the population of each country from worldbank data"""
        w_pop = WorldPopulation().get_population_df()
        self.df_csse = self.df_csse.merge(w_pop, how="left", left_on="CODE",
                                          right_on="ISO_Alpha_3_Country")

    def calculate_relative_kpi(self, df: pd.DataFrame,
                               columns: Union[List[str], str],
                               scale_key: str,
                               population_clmn: str) -> pd.DataFrame:
        """ returns a dataframe with that takes the kpis specified
        and calculates additional kpis based on the scale specified """
        # parameter assertions
        assert (isinstance(columns, str) | isinstance(columns, list)), "Columns \
                                                         must string or list of string"
        SCALE_MAPPING = {"10k": 10000,
                         "100k": 100000,
                         "1m": 1000000}
        assert (scale_key in SCALE_MAPPING), "use only specified scales, \
                                                i.e. 10k, 100k, or 1m"

        # handle based on type
        columns_type = type(columns)

        if columns_type == "str":  # convert to list
            columns = [columns]
        else:
            pass

        for kpi in columns:
            scaled_kpi = kpi + "_" + scale_key  # create scaled KPI name
            df[scaled_kpi] = df[kpi] / df[population_clmn] \
                * SCALE_MAPPING[scale_key]
        return df
    
    def getCSSEData(self):
        return self.df_csse

    @staticmethod
    def getColumnIndex(l: list, column: str):
        for i in range(len(l)):
            if l[i] == column:
                return i
        raise Exception

    def preprocessCSSEData(self, df: pd.DataFrame):
        df.drop(['Lat', 'Long', 'Province/State'], axis=1, inplace=True)
        df_csse_columns = df.columns.tolist()
        df = df.melt(id_vars=['Country/Region'],
                     value_vars=df_csse_columns[2:],
                     var_name='Date',
                     value_name='Confirmed_Cases')
        df['Date'] = pd.to_datetime(df['Date'])  # convert date to datetime
        return df

    def calcDailyNewCases(self, df: pd.DataFrame, l_groupby: list):
        df['New_Cases'] = np.nan
        df = df.groupby(l_groupby,
                        as_index=False,
                        dropna=False).sum()

        df['New_Cases'] = df.groupby(['Country/Region'], as_index=False, dropna=False)['Confirmed_Cases'].diff().fillna(
            0)
        return df

    @staticmethod
    def prep_columns_parameter(columns: Union[List[str], str]) -> List:
        # parameter assertions
        assert (isinstance(columns, str) | isinstance(columns, list)), "Columns \
                                                                 must string or list of string"
        columns_type = type(columns)
        if columns_type == "str":  # convert to list
            return [columns]
        else:
            return columns

    def calc_sma(self, df: pd.DataFrame, columns: Union[List[str], str],
                 period: int) -> pd.DataFrame:
        """ returns dataframe with new KPI applied with SMA for specified period  """
        # parameter assertions
        columns = self.prep_columns_parameter(columns)

        for kpi in columns:
            sma_kpi = kpi + "_SMA_" + str(period)  # create scaled KPI name
            df[sma_kpi] = np.nan
            df[sma_kpi] = df.groupby(['Country/Region'], dropna=False)[kpi].rolling(window=period).mean().values
            df.loc[df[sma_kpi].isnull(), sma_kpi] = 0.0

        return df

    def calc_incidence(self, df: pd.DataFrame, kpi: str = "New_Cases", period: int = 7) -> pd.DataFrame:
        """ calculates the incidence of based on the provided kpi
        (default: "New_Cases") for the period provided
        (default: 7 days) """
        incidence_kpi = kpi + "_Incidence_" + str(period)
        df[incidence_kpi] = np.nan
        df[incidence_kpi] = df.groupby(['Country/Region'],
                         dropna=False,
                         as_index=False)\
                      .apply(lambda x: 1e5*x[kpi].rolling(window=period).sum()\
                        /x.iloc[0]['Country_Population']).reset_index(drop=True)
        df[incidence_kpi].fillna(0, inplace=True)
        return df

    @staticmethod
    def countries_exceeding_threshold(df: pd.DataFrame,
                                      kpi: str = 'New_Cases_Incidence_7',
                                      threshold: int = 35) -> list:
        """ returns countries exceeding threshold """
        countries = list(df['COUNTRY'][df[kpi] > threshold].unique())
        return countries
        
    def calc_percentage_change(self, df: pd.DataFrame, columns: Union[List[str], str],
                               period: int) -> pd.DataFrame:
        """ calculates the percentage change of supplied KPI in columns compared to period days before
        returns dataframe """
        # parameter assertions
        columns = self.prep_columns_parameter(columns)
        counter = 1
        for kpi in columns:
            kpi_growth = kpi + "_PC_" + str(period)
            df[kpi_growth] = (df.groupby(['Country/Region'], as_index=False,
                                         dropna=False).apply(lambda x: x[kpi].div(x[kpi].shift(period))).values - 1) * 100
            df[kpi_growth].replace([np.inf,-np.inf], 0, inplace=True)

        return df

    def loadDf(self, f_path):
        try:
            df = pd.read_csv(f_path)
        except IOError:
            print(f'Could not read file at {f_path}')
        return df

    def remap(self, df: pd.DataFrame, column: str, mapping: dict):
        return df.replace({column: mapping})

    @staticmethod
    def getKpiTitle(d: dict, key: str):
        if key in d:
            return d[key]
        else:
            return "KPI"

    @staticmethod
    def getTickSuffix(d: dict, key: str):
        if key in d:
            return d[key]
        else:
            return ""


def getHoverText(tgt_name: str, tgt_parent: str, tgt_cntry: str, tgt_city: str,
                 src_parent: str, src_cntry: str, src_city: str):
    text = '<b>' + tgt_name + '</b>' \
                              '<br>Target Information:<br>' + \
           '<b>Target Parent Co.: </b>' + \
           tgt_parent + '<br>' + \
           '<b>Target Country: </b>' + \
           tgt_cntry + '<br>' + \
           '<b>Target City: </b>' + \
           tgt_city + '<br>' + \
           '<br>Source Information:<br>' + \
           '<b>Source Parent Co.: </b>' + \
           src_parent + '<br>' + \
           '<b>Source Country: </b>' + \
           src_cntry + '<br>' + \
           '<b>Source City: </b>' + \
           src_city
    return text


def setHoverTextVariables(row: pd.Series):
    # target hover values
    tgt_parent = "-"
    tgt_name = "-"
    tgt_cntry = "-"
    tgt_city = "-"
    # source hover values
    src_parent = "-"
    src_name = "-"
    src_cntry = "-"
    src_city = "-"

    if str(row['Parent_Organization_Name_Tgt']) != 'nan':  # parent
        tgt_parent = str(row['Parent_Organization_Name_Tgt'])
    if str(row['Account_Name_Tgt']) != 'nan':  # name
        tgt_name = str(row['Account_Name_Tgt'])
    if str(row['ISO_Country_Tgt']) != 'nan':  # country
        tgt_cntry = str(row['ISO_Country_Tgt'])
    if str(row['City_Tgt']) != 'nan':  # city
        tgt_city = str(row['City_Tgt'])
    if str(row['Parent_Organization_Name_Src']) != 'nan':  # parent
        src_parent = str(row['Parent_Organization_Name_Src'])
    if str(row['Account_Name_Src']) != 'nan':  # name
        src_name = str(row['Account_Name_Src'])
    if str(row['ISO_Country_Src']) != 'nan':  # country
        src_cntry = str(row['ISO_Country_Src'])
    if str(row['City_Src']) != 'nan':  # city
        src_city = str(row['City_Src'])

    return (tgt_parent, tgt_name, tgt_cntry, tgt_city,
            src_parent, src_name, src_cntry, src_city)


def setCountryColors(df: pd.DataFrame, col_dict) -> dict:
    cntry_list = df['ISO_Country_Src'].unique()
    countries = {i: cntry_list[i] for i in range(0, len(cntry_list))}
    country_colors = {}
    for key in countries:  # color setting by country dict setup
        country_colors[countries[key]] = col_dict[key]
    return country_colors


def setTrimsGeoCoordinates(row: pd.Series):
    longs = list()
    lats = list()
    # set longitude for end and start point
    longs.append(row['Long_Tgt'])
    longs.append(row['Long_Src'])
    # set latitude for end and start point
    lats.append(row['Lat_Tgt'])
    lats.append(row['Lat_Src'])

    return longs, lats


def createBaseChoropleth(df: pd.DataFrame, countries: dict, lctns: str, kpi: str, center: dict, zm: int,
                         title: str):
    """ creates basic choropleth_mapbox and sets default layout
    returns figure
    input dataframe (Pandas df), countries (dict), locations (str), kpi (str), center (dict), zoom (int) """
#    df[kpi] =  np.log(df[kpi])
    fig = px.choropleth_mapbox(df, geojson=countries,
                               locations=lctns,
                               color=kpi,
                               mapbox_style='light',
                               color_continuous_scale='Bluered',
                               range_color=(df[kpi].min(), df[kpi].max()),
                               zoom=zm,
                               labels = constants.KPI_DICT,
                               center=center,
                               opacity=0.3,
                               )
    fig.update_layout(
                      title={'text': title,
                             'y': 0.99,
                             'x': 0.5,
                             'xanchor': 'center',
                             'yanchor': 'top'}
    )

    return fig


class Dash_Helper(object):
    def __init__(self):
        pass

    @staticmethod
    def generate_dropdown_options(df: pd.DataFrame, column: str) -> list:
        """ creates options list of dictionaries for dropdown menu based 
        on provided dataframe (pandas) and column (string) """
        return [{"label": x, "value": x} for x in df[column].unique()]



if __name__ == "__main__":
    pd.set_option('display.max_columns', None)
    pd.set_option('display.max_colwidth', -1)
    # covid = CSSE_COVID_19()
    # df = covid.getCSSEData()
    # print(df['New_Cases_Incidence_7'].isna().describe())
    # print(df.dtypes)
    # print(f"Threshold 35: {len(covid.countries_exceeding_threshold(df))}")
    # print(f"Threshold 50: {len(covid.countries_exceeding_threshold(df, threshold=50))}")

    
    # print(f"DF columns: {df.head(5)}")
    # min_nc = df["New_Cases_100k"].min()
    # max_nc = df["New_Cases_100k"].max()
    # print(f"DF - New Cases Min:{min_nc} , Max: {max_nc}\n DF - Confirmed Cases Min:\
    # { df['Confirmed_Cases_100k'].min() }, Max: { df['Confirmed_Cases_100k'].max() }")
    # df.to_csv("./csse_kpi.csv", index=False)
    sn = SupplierNetwork(dtype_df=constants.DTYPE_GFN, dtype_trims=constants.DTYPE_TRIMS,
                           f_src=constants.F_SRC, cntry_mapping=constants.COUNTRY_MAPPING,
                           separator=constants.SEP, decimal=constants.DECIMAL)
    # print(f"Supply Network:\n {sn.df}\n"
    #       f"Supply Network Dtypes:\n {sn.df.dtypes}\n"
    #       f"Trims Network:\n {sn.trims_network}\n"
    #       f"Trims Network Dtypes:\n {sn.trims_network.dtypes}")
