#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 14:54:35 2020

@author: philipp
"""
__version__ = '1.0'
__author__ = 'SciScry GmbH'
# system imports
#import copy
import re
from datetime import datetime as dt
from datetime import timedelta
import io
import base64

# external packages
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_table
import dash_table.Format as dash_format
import plotly.graph_objects as go
import plotly.express as px
from dash.dependencies import Input, Output
import pandas as pd
from flask_caching import Cache
from flask import send_file
#import seaborn as sns

# local modules
#from globalfactorynetwork import *
import globalfactorynetwork as gfn
import constants


# set dash app
app = dash.Dash(__name__, external_stylesheets=constants.EXTERNAL_STYLESHEETS)
#server = app.server

# assume you have a "wide-form" data frame with no index
# see https://plotly.com/python/wide-form/ for more options

# get main dataframe for analysis


def createScatterMapbox(df, continent: list, prd_cat: list, hover: list, zm: int, title: str):
    fig = px.scatter_mapbox(df[df.Continent.isin(continent) &
                               df.Product_Categories.eq(prd_cat[0])],
                            lat='Lat', lon='Long',
                            size='Workers_Count',
                            color='Migrant_Percentage',
                            color_continuous_scale='Bluered',
                            hover_name='Account_Name',
                            hover_data=hover,
                            labels={
                                'Migrant_Percentage': 'Migrant Workers in %'},

                            zoom=zm)
    fig.update_layout(
        #                    height=700,
        #                    width=900,
        title={'text': title,
               'y': 0.96,
               'x': 0.5,
               'xanchor': 'center',
               'yanchor': 'top'},
        #                      margin=constants.MARGIN
    )
    return fig


def data_bars(df, column):
    n_bins = 100
    bounds = [i * (1.0 / n_bins) for i in range(n_bins + 1)]
    ranges = [
        ((df[column].max() - df[column].min()) * i) + df[column].min()
        for i in bounds
    ]
    styles = []
    for i in range(1, len(bounds)):
        min_bound = ranges[i - 1]
        max_bound = ranges[i]
        max_bound_percentage = bounds[i] * 100
        styles.append({
            'if': {
                'filter_query': (
                    '{{{column}}} >= {min_bound}' +
                    (' && {{{column}}} < {max_bound}' if (
                        i < len(bounds) - 1) else '')
                ).format(column=column, min_bound=min_bound, max_bound=max_bound),
                'column_id': column
            },
            'background': (
                """
                    linear-gradient(90deg,
                    #0074D9 0%,
                    #0074D9 {max_bound_percentage}%,
                    white {max_bound_percentage}%,
                    white 100%)
                """.format(max_bound_percentage=max_bound_percentage)
            ),
            'paddingBottom': 2,
            'paddingTop': 2
        })

    return styles


def addTraces(df: pd.DataFrame, figure: go.Figure):
    # define colors for all countries
    country_colors = gfn.setCountryColors(df, constants.COLORS)
    # create trace data for factories and create trace
    for index, row in df.iterrows():
        # create longitude list
        longs, lats = gfn.setTrimsGeoCoordinates(row)
        # create Text
        name = list()
        name.append(row['Account_Name_Tgt'])
        # create hover text
        hov_text = list()
        # target hover values
        tgt_parent, tgt_name, tgt_cntry, \
            tgt_city, src_parent, src_name, \
            src_cntry, src_city = gfn.setHoverTextVariables(row)
        # creating hover text string
        text_str = gfn.getHoverText(tgt_name, tgt_parent, tgt_cntry, tgt_city,
                                    src_parent, src_cntry, src_city)
        hov_text.append(text_str)
        # update figure
        fig_trim = go.Figure(go.Scattermapbox(
            mode="markers+lines+text",
            lon=longs,
            lat=lats,
            hovertext=hov_text,
            hoverinfo='text',
            showlegend=False,
            text=name,
            opacity=0.75,
            marker={'size': 11,
                    'symbol': 'triangle',
                    'color': country_colors[row['ISO_Country_Src']]}))
        # add created trace to figure
        figure.add_trace(fig_trim.data[0].update())
    return figure


CACHE_CONFIG = {"CACHE_TYPE": "filesystem",
                "CACHE_DIR": "cache-directory"}

cache = Cache(app.server, config=CACHE_CONFIG)

server = app.server

app.config.suppress_callback_exceptions = True

app.layout = html.Div(
    children=[html.Div(id='page-container',
                       children=[dbc.Row(id='header',
                                         children=[dbc.Col(width=3),
                                                   dbc.Col(children=[html.H1(children='adidas Global Factory Dashboard',
                                                                             style={"text-align": "center"}),
                                                                     html.Div(children='Overview of worldwide adidas\
                                                                     supplier factories including revenue and risk\
                                                                     impact of Covid-19.',
                                                                              style={'text-align': 'center'})],
                                                           ),
                                                   dbc.Col(html.Img(src=app.get_asset_url('logo.png'),
                                                                    style={'height': '30%'}),
                                                           width=3, style={'textAlign': 'right',
                                                                           "margin": "5px"})
                                                   ],
                                         style={"height": "100px"}
                                         ),
                                 dbc.Row(id='selector-row', className='border border-secondary',
                                         justify='around',
                                         children=[dbc.Col(id='region-selector',
                                                           width=2,
                                                           children=[html.P("Select Region"),
                                                                     dcc.RadioItems(id="continent-select",
                                                                                    options=[
                                                                                        {"label": "Americas",
                                                                                         "value": "americas"},
                                                                                        {"label": "EMEA",
                                                                                         "value": "emea"},
                                                                                        {"label": "Asia",
                                                                                         "value": "asia"}
                                                                                    ],
                                                                                    labelStyle={
                                                                                        'display': 'block'},
                                                                                    value="americas"),
                                                                     ]
                                                           ),
                                                   dbc.Col(id='category-selector',
                                                           width=2,
                                                           children=[html.P("Select Category"),
                                                                     dcc.RadioItems(id="category-select",
                                                                                    options=[
                                                                                        {"label": "Apparel",
                                                                                         "value": "ap"},
                                                                                        {"label": "Footwear",
                                                                                         "value": "fw"}
                                                                                    ],
                                                                                    labelStyle={
                                                                                        'display': 'block'},
                                                                                    value="ap"),
                                                                     ]
                                                           ),
                                                   dbc.Col(id='date-selector',
                                                           width=2,
                                                           children=[html.P("Select Covid Date"),
                                                                     dcc.DatePickerSingle(id="date-picker",
                                                                                          min_date_allowed='2020-01-22',
                                                                                          max_date_allowed=dt.today() - timedelta(days=1),
                                                                                          initial_visible_month=dt.today() - timedelta(days=1),
                                                                                          date=dt.today() - timedelta(days=1)),
                                                                     ]
                                                           ),
                                                   dbc.Col(id='covid-selector',
                                                           width=2,
                                                           children=[html.P("Select Covid KPI"),
                                                                     dcc.Dropdown(id="covid-kpi-select",
                                                                                  options=constants.DROPDOWN_KPIS,
                                                                                  value="New_Cases_Incidence_7",
                                                                                  style={
                                                                                      "display": "block"},
                                                                                  )
                                                                     ]
                                                           ),
                                                   dbc.Col(id='trims-selector',
                                                           width=2,
                                                           children=[html.P("Select Trims Factory"),
                                                                     dcc.Dropdown(id="trims_dropdown",
                                                                                  style={"display": "block"
                                                                                         })
                                                                     ]
                                                           )
                                                   ]
                                         ),
                                 dbc.Row(id='supplier-row',
                                         children=[dbc.Col(dcc.Graph(id='supplier-locations'),
                                                           width=6),
                                                   dbc.Col(dash_table.DataTable(id='financial-impact',
                                                                                fixed_rows={
                                                                                    'headers': True},
                                                                                page_action='none',
                                                                                sort_action='native',
                                                                                style_table={
                                                                                            'overflowY': "auto"},
                                                                                style_cell={
                                                                                    'textAlign': 'left',
                                                                                    'padding': '5px',
                                                                                    'textOverflow': 'ellipsis',
                                                                                    'maxWidth': 0
                                                                                },
                                                                                sort_by=[{"column_id": 'Revenue',
                                                                                          "direction": "desc"}],
                                                                                css=[{'selector': '.row', 'rule': 'margin: 0'},
                                                                                     {'selector': '', 'rule': 'margin: 20px'}]
                                                                                #                                                                                {'selector': '', 'rule': 'margin-bottom: 20px'},
                                                                                #                                                                                {'selector': '', 'rule': ''}]
                                                                                ),
                                                           width=6)
                                                   ],
                                         justify='around'
                                         ),
                                 dbc.Row(id='trims-row',
                                         justify='around',
                                         children=[dbc.Col(dcc.Graph(id='trims-map'),
                                                           width=6),
                                                   dbc.Col(dcc.Graph(id='scenario-variations'),
                                                           width=6)
                                                   ]
                                         ),
                                 dbc.Row(id='histo-row',
                                         justify='around',
                                         children=[dbc.Col(dcc.Graph(id='trims-histogram'),
                                                           width=6),
                                                    dbc.Col(dash_table.DataTable(id='risk-assessment',
                                                                                fixed_rows={
                                                                                   'headers': True},
                                                                                page_action='none',
                                                                                sort_action='native',
                                                                                style_table={
                                                                                    'overflowY': "auto"},
                                                                                style_cell={
                                                                                    'textAlign': 'left',
                                                                                    'padding': '5px',
                                                                                    'textOverflow': 'ellipsis',
                                                                                    'maxWidth': 0
                                                                                },
                                                                                sort_by=[{"column_id": 'New_Cases_Incidence_7',
                                                                                          "direction": "desc"}],
                                                                                css=[{'selector': '.row', 'rule': 'margin: 0'},
                                                                                     {'selector': '', 'rule': 'margin: 20px'}]
                                                                                ),
                                                           width=6,
                                                           )
                                                   ]
                                         )
                                 ]
                       )
              ]
)


@cache.memoize(timeout=3600)
def get_covid_data(date: str):
    df_covid = gfn.CSSE_COVID_19().getCSSEData()
    return df_covid[df_covid['Date'] == date]


@cache.memoize()
def get_countries():
    return gfn.CountriesGeoJSON().getCountriesGeoJSON()


@cache.memoize()
def get_supply_network():
    return gfn.SupplierNetwork(dtype_df=constants.DTYPE_GFN, dtype_trims=constants.DTYPE_TRIMS,
                               f_src=constants.F_SRC, cntry_mapping=constants.COUNTRY_MAPPING,
                               separator=constants.SEP, decimal=constants.DECIMAL)


@cache.memoize()
def get_factories_df(supply_network):
    df = supply_network.get_df()
    return df


@cache.memoize()
def get_trims_list(continent_sel: str, category_sel: str):
    trims_config = list()
    trims_dataset = dict()
    # get selection data from preset
    for continent in constants.CONTINENT_PRESET:
        for category in constants.CONTINENT_PRESET[continent]:
            trims_sel_component = (continent, category,
                                   constants.CONTINENT_PRESET[continent][category][0])
            trims_config.append(trims_sel_component)
    sn = get_supply_network()
    for elm in trims_config:
        df = sn.getTrimsNetwork(elm[2].copy())
        trims_l = df["Account_Name_Src"].unique().tolist()
        if not trims_dataset.get(elm[0], None):
            temp_d = {elm[1]: trims_l}
            trims_dataset[elm[0]] = temp_d
        else:  # continent already exists in dict
            trims_dataset[elm[0]][elm[1]] = trims_l
    return trims_dataset[continent_sel][category_sel]


@app.callback(
    [Output("trims_dropdown", "options"),
     Output("trims_dropdown", "value")],
    [Input("continent-select", "value"),
     Input("category-select", "value")]
)
def generate_trims_values(continent: str, category: str) -> list:
    """ return options for dropdown selection """
    trims_l = get_trims_list(continent, category)
    return [{"label": x, "value": x} for x in trims_l], trims_l[0]


@app.callback(
    [Output("supplier-locations", "figure"),
     Output("trims-map", "figure"),
     Output("trims-histogram", "figure"),
     Output("financial-impact", "columns"),
     Output("financial-impact", "data"),
     Output("financial-impact", "style_data_conditional"),
     Output("risk-assessment", "columns"),
     Output("risk-assessment", "data"),
     Output("risk-assessment", "style_data_conditional"),
     Output("scenario-variations", 'figure')
     ],
    [Input("continent-select", "value"),
     Input("category-select", "value"),
     Input("date-picker", "date"),
     Input("covid-kpi-select", "value"),
     Input("trims_dropdown", "value")]
)
def render_content(continent: str, category: str, date, covid_kpi: str, trims: str) -> list:
    # organize base data
    date_sel = date.split('T')[0]
    dff = get_covid_data(date_sel)
    sel, center, zoom = get_continent_selection(continent, category)
    sn = get_supply_network()
    continent_selection = constants.CONTINENT_MAPPING[continent]
    category_selection = constants.CATEGORY_MAPPING[category]

    # create trims map
    trims_sel = sel.copy()

    trims_sel.update({"Account_Name_Src": [trims]})

    df_trims = sn.getTrimsNetwork(trims_sel.copy())

    countries = get_countries()
    title = str(trims) + " - Supply Chain Links"
    title = generate_fig_title(suffix=title)
    fig = gfn.createBaseChoropleth(dff, countries, "CODE", covid_kpi, center,
                                   zoom, title)
    trims_map = addTraces(df_trims, fig)

    # create locations
    cont_sel = constants.CONTINENT_MAPPING[continent]
    #print('unique conts', sn.df.Continent.unique())
    df = sn.df.loc[sn.df['Continent'].isin(cont_sel)]
    title = generate_fig_title(continent, category, "Factories")
    supplier_fig = createScatterMapbox(df, continent_selection, category_selection,
                                       constants.SCATTER_HOVER, zoom, title)

    # create trims histogram
    df_trims_hist = sn.getTrimsNetwork(sel.copy())
    trims_suffix = "- No. of dependent suppliers per Trims Factory"
    trims_title = generate_fig_title(continent=continent,
                                     suffix=trims_suffix)
    #print(trims_title)
    trims_hist = createSrcHistFig(
        df_trims_hist, "Account_Name_Src", trims_title)
    # columns for financial impact
    fin_imp_cols = [{'name': 'Factory', 'id': 'Account_Name',
                     'type': 'text'},
                    {'name': "Revenue (€)", 'id': "Revenue",
                     'type': 'numeric', 'format': dash_format.Format(
                         group=dash_format.Group.yes,
                         precision=2,
                         scheme=dash_format.Scheme.fixed,
                         sign=dash_format.Sign.default,
                         symbol=dash_format.Symbol.no,
                     )},
                    {'name': 'Operating Margin (€)', 'id': "Operating_Margin",
                     'type': 'numeric', 'format': dash_format.Format(
                         group=dash_format.Group.yes,
                         precision=2,
                         scheme=dash_format.Scheme.fixed,
                         sign=dash_format.Sign.default,
                         symbol=dash_format.Symbol.no,
                     )}]

    fin_data = df[[x['id'] for x in fin_imp_cols]]
    fin_bars = data_bars(fin_data, 'Revenue') + data_bars(fin_data, 'Operating_Margin') + \
        [
        {
            'if': {'column_id': "Account_Name"},
            'width': "50%",
            'textAlign': "center"
        },
        {
            'if': {'column_id': "Revenue"},
            'width': "25%"
        },
        {
            'if': {'column_id': "Operating_Margin"},
            'width': "25%"
        }
    ]

    covid_cols = [{'name': 'Country', 'id': 'Country/Region',
                   'type': 'text'},
                  {'name': "Confirmed case/100k", 'id': "Confirmed_Cases_100k",
                   'type': 'numeric', 'format': dash_format.Format(
                       group=dash_format.Group.yes,
                       precision=2,
                       scheme=dash_format.Scheme.fixed,
                       sign=dash_format.Sign.default,
                       symbol=dash_format.Symbol.no,
                   )},
                  {'name': 'New Cases/100k', 'id': "New_Cases_100k",
                   'type': 'numeric', 'format': dash_format.Format(
                       group=dash_format.Group.yes,
                       precision=2,
                       scheme=dash_format.Scheme.fixed,
                       sign=dash_format.Sign.default,
                       symbol=dash_format.Symbol.no,
                   )},
                  {'name': 'New Cases Incidence 7d.', 'id': "New_Cases_Incidence_7",
                   'type': 'numeric', 'format': dash_format.Format(
                       group=dash_format.Group.yes,
                       precision=2,
                       scheme=dash_format.Scheme.fixed,
                       sign=dash_format.Sign.default,
                       symbol=dash_format.Symbol.no,
                   )},

                  ]

    covid_fin = df[['Account_Name', 'ISO_Alpha_3_Country',
                    "Revenue", 'Operating_Margin',
                    'Tier_Level']].merge(dff,
                                         on='ISO_Alpha_3_Country')

    covid_bars = data_bars(covid_fin, 'New_Cases_Incidence_7') + data_bars(covid_fin, 'New_Cases_100k') + \
        data_bars(covid_fin, 'Confirmed_Cases_100k') + [
        {
            'if': {'column_id': "Country/Region"},
            'width': "25%",
            'textAlign': "center"
        },
        {
            'if': {'column_id': "Confirmed_Cases_100k"},
            'width': "25%"
        },
        {
            'if': {'column_id': "New_Cases_100k"},
            'width': "25%"
        },
        {
            'if': {'column_id': "New_Cases_Incidence_7"},
            'width': "25%"
        }
    ]

#    covid_fin['at_risk'] = (covida_fin['New_Cases_Incidence_7'] >= 50)
    s, bins = pd.cut(covid_fin['New_Cases_Incidence_7'], 10,
                     labels=False,
                     retbins=True,
                     precision=0)
    #print("bins:", bins)
    #    s = s.astype('category')
#    s = s.cat.rename_categories({i: 'R < {:.2f}'.format(b) for i, b in enumerate(bins)})
    covid_fin['Incidence_Buckets'] = s

    risk_scenario = createScenarioVariation(covid_fin[['Revenue',
                                                       'Operating_Margin',
                                                       'New_Cases_Incidence_7',
                                                       'Account_Name',
                                                       'Tier_Level',
                                                       'Country/Region']])

    return supplier_fig, trims_map, trims_hist, \
        fin_imp_cols, fin_data.to_dict("records"), fin_bars, \
        covid_cols, covid_fin[[x['id'] for x in covid_cols]].drop_duplicates().to_dict('records'), \
        covid_bars, risk_scenario


def createSrcHistFig(df: pd.DataFrame, criteria: str, hist_title: str):
    """expects trims selection filter and criteria for histogram"""
    fig = go.Figure(
        data=[go.Histogram(y=df[criteria].sort_values(ascending=False))])
    fig.update_layout(title={"text": hist_title,
                             'y': 0.9,
                             'x': 0.5,
                             'xanchor': 'center',
                             'yanchor': 'top',
                             'font': {'family': 'Courier New',
                                      'size': 23}}
                      )
    fig.update_yaxes(tickangle=-45)
    return fig

def createScenarioVariation(df: pd.DataFrame) -> px.histogram:
    """ alternative to histogram expecting trims selection """
    df = df[df['Tier_Level'] == 'T1']
    histo = px.scatter(df, size='Revenue',
                       color='New_Cases_Incidence_7',
                       color_continuous_scale='Bluered',
                       x='Operating_Margin', y='Country/Region',
                         labels={'Revenue': 'Revenue (€)',
                                 'Operating_Margin': 'Operating Margin',
                                 'Account_Name': 'Account Name',
                                 'New_Cases_Incidence_7': 'New Cases Incidence (7 days)'},
                         title='Revenue/Operating Margin at Risk',
                         hover_data=['Country/Region', 'Account_Name'])
    return histo


def generate_fig_title(continent: str = None, category: str = None,
                       suffix: str = None):
    title = ""
    if continent in constants.TITLE_MAPPING:
        title = constants.TITLE_MAPPING[continent]

    if category in constants.TITLE_MAPPING:
        title += " " + constants.TITLE_MAPPING[category]

    if suffix is not None:
        title += " " + suffix
    return title


def get_continent_selection(continent: str, category: str) -> dict:
    """ returns the selections parameter for the respective continent, category combination """
    if continent in constants.CONTINENT_PRESET:
        if category in constants.CONTINENT_PRESET[continent]:
            return constants.CONTINENT_PRESET[continent][category]
        else:
            raise ValueError
    else:
        raise ValueError


def getSelectedCovidData(df_covid: pd.DataFrame, date: dt.strptime, kpi: str):
    """generate CoVid dataframe, apply selection, create hover data and return dataframe"""
    assert kpi is not None, "kpi parameter cannot be None"
    # load data frame if not provided dataframe
    if df_covid is None:
        df_covid = gfn.CSSE_COVID_19().getCSSEData()

    # convert selection to applicable date
    if date is not None:
        date = dt.strptime(re.split("T| ", date)[0], ("%Y-%m-%d"))

    # apply selection to dataframe
    df_covid = df_covid.loc[(df_covid['Date'] == date)]

    df_covid['hover_text'] = df_covid["Country/Region"] + \
        ": " + round(df_covid[kpi], 2).apply(str)
    return df_covid

    # def create_Heatmap(df: pd.DataFrame, kpi: str):
    #     s = io.BytesIO()
    #     ax = sns.heatmap(df[['Account_Name', kpi]], cmap='hot', annot=True)
    #     ax.savefig(s, format='png', bbox_inches='tight')
    #     s = base64.b64encode(s.getvalue()).decode('utf-8').replace('\n', '')
    #     return '<img align="left" src="data:image/png;base64,%s">' %s


if __name__ == '__main__':
    app.run_server(port=8051, debug=False)
