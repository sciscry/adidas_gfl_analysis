import numpy as np
import plotly.express as px
import dash_bootstrap_components as dbc
import os
import socket


# Covid Data
RAW_CSSE_PATH = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'
COUNTRY_CODES_PATH = 'https://raw.githubusercontent.com/plotly/datasets/master/2014_world_gdp_with_codes.csv'
CNTRY_GEOJSON = 'https://raw.githubusercontent.com/datasets/geo-countries/master/data/countries.geojson'

CSSE_CNTRY_CLMN = 'Country/Region'
# Dash settings
hostname = "PB_MB"  # socket.gethostname()
if hostname == "PB_MB":
    print("Local Version - PB_MB")
    px.set_mapbox_access_token(open("./.mapbox_token").read())
else:
    # env variable set on heroku
    px.set_mapbox_access_token(os.environ['MAPBOX_TOKEN'])


# Threshold values
EPSILON = 0.01

# csv constants
F_SRC = "./assets/gfl_adidas_supplier_v3.csv"  # source file
F_POP = "./assets/world_bank_population.csv"  # world bank population file
SEP = ";"  # csv separator
DECIMAL = "."
COUNTRY_COLUMN = 'Location'

# dataframe - data types - global factory network
DTYPE_GFN = {
    'Account_Name': str,
    'Address_1': str,
    'Address_2': str,
    'Address_3': str,
    'Category': str,
    'City': str,
    'Error': str,
    'Female_Workers': np.float32,
    'Formatted_Address': str,
    'Full_Address': str,
    'Lat': np.float64,
    'Location': str,
    'Location_Type': str,
    'Long': np.float64,
    'Migrant_Workers': np.float32,
    'Parent_Organization_Name': str,
    'Product_Categories': str,
    'Province': str,
    'Tier': str,
    'Workers_Count': int,
    'Zip_Code': str
}

# dataframe - data types -  Trims Network
DTYPE_TRIMS = {
    # Target fields
    'Account_Name_Tgt': str,
    'Address_1_Tgt': str,
    'Address_2_Tgt': str,
    'Address_3_Tgt': str,
    'Category_Tgt': str,
    'City_Tgt': str,
    'Error_Tgt': str,
    'Female_Workers_Tgt': int,
    'Formatted_Address_Tgt': str,
    'Full_Address_Tgt': str,
    'Lat_Tgt': np.float64,
    'Location_Tgt': str,
    'Location_Type_Tgt': str,
    'Long_Tgt': np.float64,
    'Migrant_Workers_Tgt': int,
    'Parent_Organization_Name_Tgt': str,
    'Product_Categories_Tgt': str,
    'Province_Tgt': str,
    'Tier_Tgt': str,
    'Workers_Count_Tgt': str,
    'Zip_Code_Tgt': str,
    'ISO_Country_Tgt': str,
    'ISO_Alpha_2_Country_Tgt': str,
    'ISO_Alpha_3_Country_Tgt': str,
    'Continent_Tgt': str,
    'Migrant_Percentage_Tgt': np.float32,
    'Tier_Level_Tgt': str,
    'Supplier_Type_Tgt': str,
    'Revenue_Tgt': np.float64,
    'Operating_Margin_Tgt': np.float64,
    'Trims_Supplier': str,
    # Source Fields
    'Account_Name_Src': str,
    'Address_1_Src': str,
    'Address_2_Src': str,
    'Address_3_Src': str,
    'Category_Src': str,
    'City_Src': str,
    'Error_Src': str,
    'Female_Workers_Src': str,
    'Formatted_Address_Src': str,
    'Full_Address_Src': str,
    'Lat_Src': np.float64,
    'Location_Src': str,
    'Location_Type_Src': str,
    'Long_Src': np.float64,
    'Migrant_Workers_Src': str,
    'Parent_Organization_Name_Src': str,
    'Product_Categories_Src': str,
    'Province_Src': str,
    'Tier_Src': str,
    'Workers_Count_Src': str,
    'Zip_Code_Src': str,
    'ISO_Country_Src': str,
    'ISO_Alpha_2_Country_Src': str,
    'ISO_Alpha_3_Country_Src': str,
    'Continent_Src': str,
    'Migrant_Percentage_Src': str,
    'Tier_Level_Src': str,
    'Supplier_Type_Src': str,
    'Revenue_Src': str,
    'Operating_Margin_Src': str
}

# mapping of countries for ISO processing
COUNTRY_MAPPING = {
    'Diamond Princess': 'Japan',
    'Western Sahara': 'Morroco',
    'West Bank and Gaza': 'West Bank',
    'North Macedonia': 'Macedonia',
    'Estwatni': 'South Africa',
    'Czechia': 'Czech Republic',
    'Bahamas': 'Bahamas, The',
    'Gambia': 'Gambia, The',
    'Korea': 'Korea, Republic of',
    'Korea, Republic of (South Korea)': 'Korea, Republic of',
    'United States of America (US)': 'United States',
    'United States of America': 'United States',
    'US': 'United States',
    'Congo (Brazzaville)': 'Congo, Republic of the',
    'Congo (Kinshasa)': 'Congo, Democratic Republic of the'
}

# mapping of columns in CSSE US dataset
CSSE_COLUMN_NAMES = {
    'Province_State': 'Province/State',
    'Country_Region': 'Country/Region',
    'iso3': 'CODE'
}

CSSE_GROUPBY = ['Country/Region', 'Date']

SMA_KPI = ["New_Cases", "New_Cases_100k"]
PERCENTAGE_CHANGE_KPI = ["New_Cases_SMA_7"]

# KPI Text Mapping
KPI_DICT = {
    "Weekly_Change_SMA_7": "Weekly Change (MA 7 days)",
    "New_Cases_SMA_7": "New Cases (MA 7 days)",
    "New_Cases": "New Cases",
    "Confirmed_Cases": "Confirmed Cases",
    "New_Cases_100k_SMA_7": "New Cases / 100k pop (MA 7 days)",
    "New_Cases_Incidence_7": "New Cases Incidence (7 days)"
}

TICK_SUFFIX = {
    "Weekly_Change_SMA_7": "%",
    "New_Cases_SMA_7": "ppl",
    "New_Cases": "ppl",
    "Confirmed_Cases": "ppl"
}

KPI_TO_SCALE = ["Confirmed_Cases", "New_Cases"]

DROPDOWN_KPIS = [
    {"label": "New Cases - 7 days Incidence",
     "value": "New_Cases_Incidence_7"},
    {"label": "New Cases",
     "value": "New_Cases"},
    {"label": "New Cases/ 100k Pop",
     "value": "New_Cases_100k"},
    {"label": "New Cases (MA 7 days)",
     "value": "New_Cases_SMA_7"},
    {"label": "New Cases  (MA 7 days)/ 100k Pop",
     "value": "New_Cases_100k_SMA_7"},
    {"label": "New Cases (MA 7 days) - Weekly Growth",
     "value": "New_Cases_SMA_7_PC_7"},
    {"label": "Confirmed Cases",
     "value": "Confirmed_Cases"},
    {"label": "Confirmed Cases/ 100k pop",
     "value": "Confirmed_Cases_100k"}
]

# chart selection values
AMERICAS = ['NA', 'SA']
EMEA = ['EU', 'AF']
ASIA = ['AS']
FOOTWEAR = ['Footwear']
APPAREL = ['Apparel']

# adidas Revenue / Profit structure
REV_OM_2019 = {
    "americas": {"Revenue": 6973000000,
                 "Operating_Margin": 1010000000},
    "emea": {"Revenue": 8031000000,
             "Operating_Margin": 1942000000},
    "asia": {"Revenue": 8032000000,
             "Operating_Margin": 2703000000}
}

# formatting for trims
MARGIN = {'l': 5, 't': 25, 'b': 5, 'r': 5}
CENTER_AMERICAS = {'lon': -84, 'lat': 21}
CENTER_EMEA = {'lon': 56, 'lat': 20}
CENTER_ASIA = {'lon': 65, 'lat': 0}
ZOOM_AMERICAS = 1.6
ZOOM_EMEA = 1.5
ZOOM_ASIA = 1.1

# selection for trims
SEL_AMERICAS_AP = {'Continent_Tgt': AMERICAS,
                   'Product_Categories_Tgt': APPAREL}
SEL_AMERICAS_FW = {'Continent_Tgt': AMERICAS,
                   'Product_Categories_Tgt': FOOTWEAR}
SEL_EMEA_AP = {'Continent_Tgt': EMEA,
               'Product_Categories_Tgt': APPAREL}
SEL_EMEA_FW = {'Continent_Tgt': EMEA,
               'Product_Categories_Tgt': FOOTWEAR}
SEL_ASIA_AP = {'Continent_Tgt': ASIA,
               'Product_Categories_Tgt': APPAREL}
SEL_ASIA_FW = {'Continent_Tgt': ASIA,
               'Product_Categories_Tgt': FOOTWEAR}

# tab mapping to continent and footwear and apparel
CONTINENT_PRESET = {"americas": {"ap": (SEL_AMERICAS_AP, CENTER_AMERICAS, ZOOM_AMERICAS),
                                 "fw": (SEL_AMERICAS_FW, CENTER_AMERICAS, ZOOM_AMERICAS)},
                    "emea": {"ap": (SEL_EMEA_AP, CENTER_EMEA, ZOOM_EMEA),
                             "fw": (SEL_EMEA_FW, CENTER_EMEA, ZOOM_EMEA)},
                    "asia": {"ap": (SEL_ASIA_AP, CENTER_ASIA, ZOOM_ASIA),
                             "fw": (SEL_ASIA_FW, CENTER_ASIA, ZOOM_ASIA)}
                    }
CONTINENT_MAPPING = {"americas": AMERICAS,
                     "emea": EMEA,
                     "asia": ASIA}

CATEGORY_MAPPING = {"ap": APPAREL,
                    "fw": FOOTWEAR}

TITLE_MAPPING = {"americas": "Americas",
                 "emea": "EMEA",
                 "asia": "Asia",
                 "ap": "Apparel",
                 "fw": "Footwear"}


# hover data
SCATTER_HOVER = ['City', 'Parent_Organization_Name', 'Migrant_Workers',
                 'Migrant_Percentage']

# mapbox information
# external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
EXTERNAL_STYLESHEETS = [dbc.themes.BOOTSTRAP,
                        "https://codepen.io/chriddyp/pen/bWLwgP.css",
                        "https://codepen.io/chriddyp/pen/brPBPO.css"
                        ]

# plotly color values:


# CSS override
#TH_STYLE = {"text-align": "center", "font-size": "1.5em"}

colors_str = 'aliceblue, antiquewhite, aqua, aquamarine, azure,\
            beige, bisque, black, blanchedalmond, blue,\
            blueviolet, brown, burlywood, cadetblue,\
            chartreuse, chocolate, coral, cornflowerblue,\
            cornsilk, crimson, cyan, darkblue, darkcyan,\
            darkgoldenrod, darkgray, darkgrey, darkgreen,\
            darkkhaki, darkmagenta, darkolivegreen, darkorange,\
            darkorchid, darkred, darksalmon, darkseagreen,\
            darkslateblue, darkslategray, darkslategrey,\
            darkturquoise, darkviolet, deeppink, deepskyblue,\
            dimgray, dimgrey, dodgerblue, firebrick,\
            floralwhite, forestgreen, fuchsia, gainsboro,\
            ghostwhite, gold, goldenrod, gray, grey, green,\
            greenyellow, honeydew, hotpink, indianred, indigo,\
            ivory, khaki, lavender, lavenderblush, lawngreen,\
            lemonchiffon, lightblue, lightcoral, lightcyan,\
            lightgoldenrodyellow, lightgray, lightgrey,\
            lightgreen, lightpink, lightsalmon, lightseagreen,\
            lightskyblue, lightslategray, lightslategrey,\
            lightsteelblue, lightyellow, lime, limegreen,\
            linen, magenta, maroon, mediumaquamarine,\
            mediumblue, mediumorchid, mediumpurple,\
            mediumseagreen, mediumslateblue, mediumspringgreen,\
            mediumturquoise, mediumvioletred, midnightblue,\
            mintcream, mistyrose, moccasin, navajowhite, navy,\
            oldlace, olive, olivedrab, orange, orangered,\
            orchid, palegoldenrod, palegreen, paleturquoise,\
            palevioletred, papayawhip, peachpuff, peru, pink,\
            plum, powderblue, purple, red, rosybrown,\
            royalblue, rebeccapurple, saddlebrown, salmon,\
            sandybrown, seagreen, seashell, sienna, silver,\
            skyblue, slateblue, slategray, slategrey, snow,\
            springgreen, steelblue, tan, teal, thistle, tomato,\
            turquoise, violet, wheat, white, whitesmoke,\
            yellow, yellowgreen'  # length = 148
colors_str = colors_str.replace(' ', '')
COLORS_LIST = colors_str.split(',')
COLORS = {i: COLORS_LIST[i] for i in range(0, len(COLORS_LIST))}
